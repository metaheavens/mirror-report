\section{Case Studies}
Constant expression types allow easily using simple value dependent types that were not easily expressing without libraries. Here are examples of libraries that one could easily develop. First, we introduce a compile time sized matrix. Second, we implement a system that keeps track of units of terms to avoid inconsistent operations. Third, we show an implementation of sized tensors. Finally, using size tensors and composition ability of constant expression types, we conclude with an example of application on convolutional neural network, frequently used for image classification.

\subsection{Matrix\label{matrix}}
Matrix is common data structures used in numerical computation. It is important to be able to implement a reliable representation of such structure. Compile time sized matrix allows to check matrix multiplication at compile time. This operation is already possible in current Scala:

\begin{lstlisting}[style=myScalastyle]
    class Matrix[X <: Int & Singleton, Y <: Int & Singleton] {
        def dot[B <: Int & Singleton](matrix: Matrix[Y, B]): Matrix[X, B] = ???
    }
\end{lstlisting}

However, a useful implementation of matrices also needs other operations, like \(flatten\). This one was not able to be implemented with \(Int\) \(\&\) \(Singleton\) type to represent size as both sizes have to be multiplied at type level.
Thanks constant expression types, one could express the result type of the \(flatten\) method as following:

\begin{lstlisting}[style=myScalastyle]
    @mirror def *(A: Int, B: Int): Int & Singleton = A * B
    class Matrix[X <: Int & Singleton, Y <: Int & Singleton](val values: List[List[Int]]) {
        def flatten: Matrix[X * Y, 1] = new Matrix[X * Y, 1](values.flatten.map(a => a :: Nil))
    }
\end{lstlisting}

The \(concatenate\) method is also a useful method. Depending on the axis, two sizes have to be equal and the two others have to be added together. Using constant expression types, we can implement a handy \(concatenate\) method that would easily track the size of the concatenated matrix:

\begin{lstlisting}[style=myScalastyle]
    //In real world example the constructor can be made private and check that X * Y == value.size at runtime for safety
    class Matrix[X <: Int & Singleton, Y <: Int & Singleton](val values: List[List[Int]]) {
        def concatX[A](m2: Matrix[A, Y]) = new Matrix[X + A, Y](values ++ m2.values)
        def concatY[B](m2: Matrix[X, B]) = {
            val indexedM1 = values.zipWithIndex
            val indexedM2 = m2.values.zipWithIndex
            val res = indexedM1.map{(a, b) => a ++ indexedM2.find((a2, b2) => b2 == b).map(_._1).get}
            new Matrix[X, Y + B](res)
        }
    }
    val m1 = new Matrix[3, 4](List(List(1,2,3,4), List(1,2,3,4), List(1,2,3,4)))
    val m2 = new Matrix[3, 4](List(List(1,2,3,4), List(1,2,3,4), List(1,2,3,4)))
    val res1: Matrix[3, 8] = m2.concatY(m1)
    val res2: Matrix[6, 4] = m2.concatX(m1)
\end{lstlisting}

\subsection{International System Unit}
In scientific computation each value is linked with a dimension. However, these values are commonly represented with doubles which do not have the dimension information. Lack of dimension information in type system can lead to bugs that can be discovered later in the development or even at runtime. For example, Mars Climate Orbiter crashed due to a dimension error \cite{wiki:space}.

Libraries like squants \cite{squants} try to track dimensions at type level. Despite this library works with many dimensions, its design lacks of flexibility. Some uncommon dimensions are not available and the design of squants needs all of the usable dimensions to be declared. Membrane permeability measure is not expressible in squants. It is expressed by \(mol.m^{-2}.s^{-1}.bar^{-1}\).

\begin{lstlisting}[style=myScalastyle]
    val test = (1 liters) / ((1 meters) * (1 meters)) / (1 seconds) / (1 bars)
    <console>:211: error: overloaded method value / with alternatives:
      (that: squants.motion.Jerk)squants.time.TimeSquared <and>
      (that: squants.time.TimeSquared)squants.motion.Jerk <and>
      (that: squants.motion.Acceleration)squants.time.Time <and>
      (that: squants.time.Time)squants.motion.Acceleration <and>
      (that: squants.motion.Velocity)Double <and>
      (that: Double)squants.motion.Velocity
     cannot be applied to (squants.motion.Pressure)
           val toto = (1 liters) / ((1 meters) * (1 meters)) / (1 seconds) / (1 bars)
\end{lstlisting}

It is possible to implement a more flexible way to track ISU with constant expression type. To know the dimension of each value at compile time one have to track a number for each unit used in the calculus that will represent the total dimension of the value. For example, if our code needs to track seconds, kilograms, moles and meters, one could encode each value's dimension in a type member with a \(Int\) \(\&\) \(Singleton\). Each type member would be the exponent of the corresponding dimension. Here how can be represented \(kg^2\) thanks this system:

\begin{lstlisting}[style=myScalastyle]
    def kg2 = new ISUContainer {
        type S = 0; type KG = 2
        type MOL = 0; type M = 0
        def value: Double = 1
    }
\end{lstlisting}

If a multiplication happens between two values, the dimensions have to be added. If it is a division, the dimension has to be subtracted. Moreover, the example implementation keeps track of the range of each unit. You can easily express grams and kilograms and operations are scaled.

\begin{lstlisting}[style=myScalastyle]
    @mirror def -(A: Int, B: Int): Int & Singleton = A - B
    abstract class ISUContainer { self =>
        //Power of each dimension
        type S <: Int & Singleton; type KG <: Int & Singleton
        type MOL <: Int & Singleton; type M <: Int & Singleton
        //Scale of each dimension
        type PS <: Int & Singleton; type PKG <: Int & Singleton
        type PMOL <: Int & Singleton; type PM <: Int & Singleton
          
        def value: Double
    }
\end{lstlisting}

\(ISUContainer\) class contains 4 types members that represent the exponent of each dimension \(S, KG, MOL, M\). There are also 4 types members that represents the scale of each value. For example, it is used to implement grams and kilograms as they are of the same dimension, but of a different scale. Grams is \(10^{-3}\) kilograms, we use these types members to encode this information at type-level. We implement an implicit class that can convert easily \(Int\) to \(ISUContainer\), for grams and kilograms:

\begin{lstlisting}[style=myScalastyle]
    implicit class IntIpv(i: Int) {
        def kg = new ISUContainer {
            type S = 0; type KG = 1
            type MOL = 0; type M = 0
            type PS = 0; type PKG = 0
            type PMOL = 0; type PM = 0
            def value: Double = i
        }
        def g = new ISUContainer {
            type S = 0; type KG = 1
            type MOL = 0; type M = 0
            type PS = 0; type PKG = 0 - 3
            type PMOL = 0; type PM = 0

            def value: Double = i
        }
    }
\end{lstlisting}

Then we need methods to multiply, divide and add our values together. We implement a \(scale\) method to rescale value if needed. Sum, division and multiplication use implicit synthesis to get the vector of scales for each value. In division method we subtract all the dimensions and in multiplication, we add them.

\begin{lstlisting}[style=myScalastyle]
    abstract class ISUContainer { self =>
        ...
        private def scale(a1: Array[Int], a2: Array[Int], toScale: Double): Double = {
            val zipped: Array[(Int, Int)] = a1 zip a2
            val sum: Array[Int] = zipped.map(t => t._2 - t._1)
            val total: Int = sum.sum
            toScale * scala.math.pow(10, total)
        }
        //Use of implicit synthesis to get scale of each dimension
        def *(v: ISUContainer)(implicit pow1: (self.PS, self.PKG, self.PMOL, self.PM),
        pow2: (v.PS, v.PKG, v.PMOL, v.PM)) =
        new ISUContainer {
            type S = v.S + self.S; type KG = v.KG + self.KG
            type MOl = v.MOL + self.MOL; type M = v.M + self.M
            type PS =  self.PS; type PKG = self.PKG
            type PMOL = self.PMOL; type PM = self.PM
            override def value = self.value * scale(pow1.toArray.map(_.asInstanceOf[Int]), pow2.toArray.map(_.asInstanceOf[Int]), v.value)
        }
        def /(v: ISUContainer)(implicit pow1: (self.PS, self.PKG, self.PMOL, self.PM),
        pow2: (v.PS, v.PKG, v.PMOL, v.PM)) =
        new ISUContainer {
            type S = v.S - self.S; type KG = v.KG - self.KG
            type MOl = v.MOL - self.MOL; type M = v.M - self.M
            type PS =  self.PS; type PKG = self.PKG
            type PMOL = self.PMOL; type PM = self.PM
            override def value = self.value * scale(pow1.toArray.map(_.asInstanceOf[Int]), pow2.toArray.map(_.asInstanceOf[Int]), v.value)
        }
        def +(v: ISUContainer)(implicit pow1: (self.PS, self.PKG, self.PMOL, self.PM),
        pow2: (v.PS, v.PKG, v.PMOL, v.PM),
        eq: (self.S, self.KG, self.MOL, self.M) =:=
        (v.S, v.KG, v.MOL, v.M)) =
        new ISUContainer {
            type S = v.S; type KG = v.KG
            type MOl = v.MOL; type M = v.M

            type PS =  self.PS; type PKG = self.PKG;
            type PMOL = self.PMOL; type PM = self.PM
            override def value = self.value + scale(pow1.toArray.map(_.asInstanceOf[Int]), pow2.toArray.map(_.asInstanceOf[Int]), v.value)
        }
    }
\end{lstlisting}

With the addition and the scaling, we are now able to create 2 values of different units and add them together.

\begin{lstlisting}[style=myScalastyle]
    def v1 = 6 kg
    def v2 = 6 g
    def res = v1 * v2 // res.value == 0.036
\end{lstlisting}

The implementation of division and multiplication let us declare the unit that was not available with squants. Seconds, moles and meters can be declared similarly to kilograms. We can create more complex unit, as demonstrated below for \(m^3\) and \(pascal\).

\begin{lstlisting}[style=myScalastyle]
    implicit class IntIpv(i: Int) {
        // Implementation of s, mol, m are here omitted.
        def m3 = new ISUContainer {
            type S = 0; type KG = 0
            type MOL = 0; type M = 3
            type PS = 0; type PKG = 0
            type PMOL = 0; type PM = 0
            def value: Double = i
        }
        def pascal = new ISUContainer { // Define easily a new unit
        type S = 0 - 2; type KG = 1
        type MOL = 0; type M = 0 - 1
        type PS = 0; type PKG = 0
        type PMOL = 0; type PM = 0
        def value: Double = i
        }
    }
    def permeability = 1.m3 / (1.m * 1.m) / 1.sec / 1.pascal // Use an unit that was not available with squants
\end{lstlisting}

Unused dimensions in this example are removed but they can be easily added to the current implementation.

\subsection{Sized Tensor Implementation}
Developing an application using deep learning frameworks with untyped languages can lead to runtime errors due to use of operations that does not work on given sized tensors. In tensorflow such errors are even more painful as they happen deep in the stack trace, making them hard to trace back. These two problems reduce library usability and therefore the programmer's productivity.

To avoid these errors we can use compile-time sized tensors. A check can be done in the types system to ensure sizes are correct. We've already seen that constant expression types are useful to express concatenation in Section \ref{matrix}. However, we are still not able to easily deal with an unknown number of dimensions that a tensor implementation implies.
Using match types we can express concatenation of tensors on whatever dimension we want.

\subsubsection{Match Types}
The next examples use match types. Match types is a new feature introduced in the Dotty compiler. It allows declaring types using pattern matching on types. Here is one of the examples available on the website \cite{matchtypes}:

\begin{lstlisting}[style=myScalastyle]
    type Concat[+Xs <: Tuple, +Ys <: Tuple] <: Tuple = Xs match {
        case Unit => Ys
        case x *: xs => x *: Concat[xs, Ys]
    }
\end{lstlisting}

Mirror methods combined with this feature make the Scala type system much more expressive. In this section we show two examples of possibility enabled by these two new features. Firstly, we implement a sized tensor representation and then we will use this tensor and recursive mirror method to express the size of convolutional neural network.

To implement the concatenation method on tensor, we have to check that all dimensions are equals but the concatenated one. We split the tuple of \(Int\) \(\&\) \(Singleton\) that represents our dimensions size to take parts that have to be equal. We use the type \(FirstN\) and \(AfterN\) to get all these type-level values.

\begin{lstlisting}[style=myScalastyle]
    type Concat[Xs1 <: Tuple, Xs2 <: Tuple] <: Tuple = Xs1 match {
        case x *: xs => x *: Concat[xs, Xs2]
        case Unit => Xs2
    }
    type FirstN[D <: Tuple, A <: Int] = FirstNRec[D, A, Unit]
    type FirstNRec[D <: Tuple, A <: Int, Acc <: Tuple] <: Tuple = A match {
        case 0 => Acc
        case Int & Singleton => D match {
            case x *: xs => Concat[FirstNRec[xs, A - 1, x *: Acc], Unit]
        }
    }
    type AfterN[D <: Tuple, A <: Int] <: Tuple = A match {
        case 0 => D
        case Int & Singleton => D match {
            case x *: xs => Concat[AfterN[xs, A - 1], Unit]
        }
    }
\end{lstlisting}

We can now check the equality of all dimensions but the concatenated one:

\begin{lstlisting}[style=myScalastyle]
    class Tensor[D_ <: Tuple](val data: List[Int]) {
        type D = D_
        def concat[A <: Int & Singleton](t2: Tensor[_])(implicit ev1: FirstN[D, A] =:= FirstN[t2.D, A], ev2: AfterN[D, A + 1] =:= AfterN[t2.D, A + 1])
    }
\end{lstlisting}

Then we need to implement the output type. The output type is the same but the concatenated dimension will be the sum of this dimension in each tensor. The \(Concatenerise\) type takes two tuple, a dimension number and return the concatenation output type.

\begin{lstlisting}[style=myScalastyle]
    type ReverseRec[D <: Tuple, Acc <: Tuple] <: Tuple = D match {
        case x *: xs => Concat[ReverseRec[xs, x *: Acc], Unit]
        case Unit => Acc
    }
    type Reverse[D <: Tuple] = ReverseRec[D, Unit]
    type Concatenerise[D1 <: Tuple, D2 <: Tuple, A <: Int, Acc <: Tuple] <: Tuple = A match {
        case 0 => D1 match {
            case x1 *: xs1 => D2 match {
                case x2 *: xs2 => x2 match {
                    x2 <: Int & Singleton => x1 match {
                        x1 <: Int & Singleton => Concat[Concat[Reverse[Acc], (x1 + x2) *: Unit], xs2]
                    }
                }
            }
        }
        case Int & Singleton => D1 match {
            case x1 *: xs1 => D2 match {
                case x2 *: xs2 => x2 match {
                    x2 <: Int & Singleton => x1 match {
                        x1 <: Int & Singleton => Concat[Concatenerise[xs1, xs2, A - 1, x1 *: Acc], Unit]
                    }
                }
            }
        }
    }
\end{lstlisting}

We are finally able to implement a fully typed concatenation method for a compile time size tensor:

\begin{lstlisting}[style=myScalastyle]
    class Tensor[D_ <: Tuple](val data: List[Int]) {
        type D = D_
        def concat[A <: Int & Singleton](t2: Tensor[_])(implicit ev1: FirstN[D, A] =:= FirstN[t2.D, A], ev2: AfterN[D, A + 1] =:= AfterN[t2.D, A + 1]): Tensor[Concatenerise[D, t2.D, A, Unit]] = ???
    }
    val tensor1 = new Tensor[(6, 2, 1, 3, 4)]
    val tensor2 = new Tensor[(6, 2, 3, 3, 4)]
    val result: Tensor[(6, 2, 4, 3, 4)] = tensor1.concat[2](tensor2)
\end{lstlisting}

This example is using match on subtypes in pattern matching on types that are not implemented when we are writing this paper but is planned \cite{matchtypes} \cite{issuematch}.

\subsection{Sized Convolutional Neural Network}
Convolutional neural network is a category of architecture in neural networks, mainly used for image recognition \cite{ronneberger2015u}. The number of layers in these architectures can change and go deeper if the use case and the computing power allows it. The size of each layer changes, usually the size of the image is divided by two and the number of filters is multiplied by two \cite{ronneberger2015u}. Being able to adapt libraries to different depths would make them handy to use.

Thanks to constant expression types and their composition ability, we can express function such power that is useful for the different layers of the architecture. Match types help to define a map method at type level and an easy way to declare list of integers. Combining that we can easily expose API that returns different types according to the depth of our network.

\begin{lstlisting}[style=myScalastyle]
    object mirrors {
        @mirror def power(I: Int, N: Int): Int = if(N == 0) 1 else I * power(I, N - 1)
    }
    import mirrors._
    type MapInt[D1 <: Tuple, F[_ <: Int]] <: Tuple = D1 match {
        case x *: xs => x match {
            x <: Int & Singleton => F[x] *: MapInt[xs, F]
        }
        case Unit => Unit
    }
    type ListInt[A <: Int] <: Tuple = A match {
        case 0 => Unit
        case Int & Singleton => A *: ListInt[A - 1]
    }
    type power2[A <: Int] = power[2, A]
    type TupleSize[A <: Int] = (A, 400, 400)
    type Layers[A] = MapInt[MapInt[ListInt[A], power2], TupleSize]
    val x1: Layers[5] = ???
    val x2: ((32, 400, 400), (16, 400, 400), (8, 400, 400), (4, 400, 400), (2, 400, 400)) = x1
    val x3: Layers[4] = ???
    val x4: ((16, 400, 400), (8, 400, 400), (4, 400, 400), (2, 400, 400)) = x3
\end{lstlisting}

In the above example, we created the type \(Layers[A <: Int\) \(\&\) \(Singleton]\) that takes the number of layers of our neural network and outputs a tuple. It can be passed to a tensor that represents the filters applied to one image of size \(400*400\). One could easily do the division for sizes the same way.
