\section{Constant Expression Types Overview\label{sec:overview}}
We reduce the gap between terms and types by introducing a new way to declare methods. These methods start with the \(@mirror\) annotation that allows to use them on types as well on terms by creating an associated higher-kinded type.

Our \(concatenate\) method that we failed to implement can be written thanks constant expression types:

\begin{lstlisting}[style=myScalastyle]
    import scala.annotation.mirror
    @mirror def +(A: Int, B: Int): Int & Singleton = A + B
    class Vector[S1 <: Int & Singleton]{
        def concatenate[S2 <: Int & Singleton](vec: Vector[S2]): Vector[S1 + S2]
    }
\end{lstlisting}

\subsection{Declaration\label{sec:declaration}}
Mirror methods look like normal methods but have additional constraints. Preceded by the \(@mirror\) annotation, they have to take only primitive types as arguments. Allowed operations inside these methods are the following:

\begin{itemize}
 \item Conditions
 \item Primitive operators
 \item Application of mirror functions
\end{itemize}

Here is a simple example of valid mirror function:

\begin{lstlisting}[style=myScalastyle]
    import scala.annotation.mirror
    object mirrors {
        @mirror def add(A: Int, B: Int): Int & Singleton = A + B
    }
\end{lstlisting}

\subsection{Introduced Type}
To be able to follow at type level the operations applied with mirror methods, we have to change the result type of these methods. To do so we implement desugaring phase that transforms mirror methods.

\begin{lstlisting}[style=myScalastyle]
    @mirror def +(A: Int, B: Int): Int = A + B // Code written
    // Desugaring
    @mirror type +[A <: Int, B <: Int] <: Int
    @mirror def +(A: Int, B: Int): +[A.type, B.type] = (A + B).asInstanceOf[+[A.type, B.type]]
\end{lstlisting}

Desugaring is a common method used to add features to a language without changing its semantic. It allows improving expressiveness of a language without breaking its soundness.

For each method declared with the \(@mirror\) annotation, a linked higher-kinded type is created, with the same number of type arguments than the number of arguments present in the original method. The method will have its output type modified by the annotation, changing it by the newly introduced higher-kinded type.

\subsection{Constant Expression Type Reduction}
When instantiated, constant expression types need particular type parameters to be able to be reduced. All of them have to refer, directly or indirectly, to a \(ConstantType\). It means that type parameters should have one of the following forms:

\begin{itemize}
 \item \(TypeRef\) that eventually refers to a \(ConstantType\)
 \item \(TermRef\) that eventually refers to a \(ConstantType\)
 \item \(ConstantType\)
\end{itemize}

Once all these conditions are fulfilled these types are evaluated at compile time according to theirs linked function. It means that an instantiated constant expression type becomes a subtype of its result and vice versa. We use an interpreter that reduces these types according to the method. Operations presented in Section \ref{sec:declaration} are implemented in this interpreter.

\begin{lstlisting}[style=myScalastyle]
    def eq[A, B](implicit eq: A =:= B) = ???
    eq[add[1, 2], 3] // compiles
    eq[add[1, 2], 4] // does not compile
\end{lstlisting}

\subsection{Composition}
It is possible to use other constant expression types in a mirror method. We can compose them to do more complex computations. It means that we can loop in constant expression types thanks to recursion.

\begin{lstlisting}[style=myScalastyle]
    @mirror def Fibo(A: Int): Int = 
     if (A == 0) 0 
     else if(A == 1) 1 
     else Fibo(A - 1) + Fibo(A - 2)
\end{lstlisting}
