\section{Implicit Synthesis}
Now that constant expression types are available we can define our \(concatenate\) method with the desired type, see Section \ref{sec:overview}. However, a new problem appears when we want to use information in the type, e.g. the size of a vector, as a value. This information is useful to create more type safe libraries using it at type level but we quickly get stuck when trying to use it at runtime. A simple example that illustrates this problem is the implementation of a \(size\) method in our vector. As the size is now only known at type level, no term can be used to extract this value.

\begin{lstlisting}[style=myScalastyle]
    class Vector[S <: Int & Singleton] {
        def size: Int = ???
    }
\end{lstlisting}

The main idea to solve this issue is to resort to implicits in Scala. We change the implicit search to synthesise an implicit argument when its type is inhabited by only one value. With the implicit synthesis, we are now able to transfer information that was only available at type level to term level. The types that can be synthesised this way are presented with their conversion in Table \ref{tab:synthesise}.

\begin {table}
\caption {Synthesise conversion} \label{tab:synthesise}
\begin{center}
\begin{tabular}{|l|c|}
      \hline
      Type & Term  \\
      \hline
      TypeVar & Synthesise of the underlying type \\
      ConstantType & Literal \\
      TermRef & Corresponding identifier \\
      ThisType & This \\
      AppliedType reducible & Synthesizes of the reduced type \\
      AppliedType mirror & Corresponding application of mirror method \\
      Tuple with reducible values & Tuple with reduced values \\
      \hline
\end{tabular}
\end{center}
\end{table}

The \(size\) is straight forward to implement using this system:

\begin{lstlisting}[style=myScalastyle]
    class Vector[S <: Int & Singleton] {
        def size(implicit s: S): Int = s
    }
    new Vector[2].size // compiles even without implicit instance of type 2 available in the scope
\end{lstlisting}
